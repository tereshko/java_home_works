import java.util.Scanner;

import static java.lang.Math.sqrt;

public class main {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);


        System.out.println("Please enter side A:");
        int sideA = in.nextInt();

        System.out.println("Please enter side B:");
        int sideB = in.nextInt();

        System.out.println("Please enter side C:");
        int sideC = in.nextInt();

        int perimeter = (sideA + sideB + sideC);
        System.out.println("Perimeter = " + perimeter);

        double area = sqrt((perimeter * (perimeter - sideA) * (perimeter - sideB) * (perimeter - sideC)));
        System.out.println("area = " + area);

    }
}
