import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        System.out.println("We are going from SF->London");
        System.out.println("Regular price cost $1000");

        int totalCost = 0;
        Scanner in = new Scanner(System.in);
        System.out.println("How old are you: ");
        int userAge = in.nextInt();

        if (userAge <= 3) {
            totalCost = 0;
        }
        if (userAge > 3 || userAge >= 15) {
            totalCost = (int) (1000- (1000 * 0.2));
        }
        if (userAge >= 65) {
            System.out.println("Are you veteran: ");
            System.out.println("1. Yes");
            System.out.println("2. No");
            System.out.print("Answer: ");
            int veteranStatus = in.nextInt();

            if (veteranStatus == 1) {
                totalCost = (int) (1000- (1000 * (0.15 + 0.1)));
            } else {
                totalCost = (int) (1000 - (1000 * 0.15));
            }
        }
        System.out.println("Pay: " + totalCost);
    }
}
