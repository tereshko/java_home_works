import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Please enter value:");
        int value = in.nextInt();

        System.out.println("1. Binary");
        System.out.println("2. Octal");
        System.out.println("3. Hex");
        System.out.println("Please select system to convert: ");
        int select = in.nextInt();
        switch (select) {
            case 1:
                System.out.println(Integer.toBinaryString(value));
                break;
            case 2:
                System.out.println(Integer.toOctalString(value));
                break;
            case 3:
                System.out.println(Integer.toHexString(value));
                break;
            default:
                System.out.println("Wrong selection");
        }
    }
}
