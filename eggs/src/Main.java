import java.util.List;
import java.util.Vector;

public class Main {
    public static void main(String[] args) {
        System.out.println("We will play to the game. Lets start!");

        PrintStatusInformation printStatusInformation = new PrintStatusInformation();
        boolean isExitRequired = false;

        List<Chicken> chickenList = new Vector();
        for (int i = 0; i <= 2; i++) {
            chickenList.add(new Chicken());
        }

        while (!isExitRequired) {
            for (int i = 0; i <= chickenList.size() - 1; i++) {
                System.out.println("Current chicken has number: " + i);
                printStatusInformation.printInfo(chickenList.get(i));
                int userStepSelected = printStatusInformation.getUserStepChoice();
                if (userStepSelected == 3) {
                    isExitRequired = true;
                    System.out.println("Exit");
                } else {
                    boolean isChickenAlive = chickenList.get(i).chickenTic(userStepSelected);
                    if (isChickenAlive == false) {
                        chickenList.remove(i);
                    }
                }
            }
            if (chickenList.size() == 0) {
                isExitRequired = true;
                System.out.println("Game ended. No chicken alive");
            }
        }
    }
}
