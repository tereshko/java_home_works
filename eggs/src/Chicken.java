public class Chicken {
    PrintStatusInformation printStatusInformation = new PrintStatusInformation();

    private boolean isChickenAlive = true;
    private int seedsCount = 4;
    private boolean isEggAvailable = false;
    private int eggsCount = 0;
    private int stepCount = 0;

    public int getSeedsCount() {
        return seedsCount;
    }

    public int getStepCount() {
        return stepCount;
    }

    public boolean getIsChickenAlive() {
        return isChickenAlive;
    }

    public boolean getIsEggAvailable() {
        return isEggAvailable;
    }

    public int getEggsCount() {
        return eggsCount;
    }

    public boolean chickenTic(int userStepSelected) {

        switch (userStepSelected) {
            case 1:
                System.out.println("Added 5 seeds");
                seedsCount = seedsCount + 5;
                break;
            case 2:
                if (isEggAvailable == false) {
                    System.out.println("Sorry, no egg");
                } else {
                    System.out.println("Took 1 egg");
                    eggsCount = eggsCount + 1;
                    isEggAvailable = false;
                }
                break;
            default:
                System.out.println("Wrong answer");
                break;
        }

        if (seedsCount == 0) {
            isChickenAlive = false;
        }

        stepCount = stepCount + 1;

        if (stepCount == 4) {
            isEggAvailable = true;
            stepCount = 0;
        }

        seedsCount = seedsCount - 1;

        if (isChickenAlive == false) {
            System.out.println("Chicken is dead");
        }
        return isChickenAlive;
    }
}
