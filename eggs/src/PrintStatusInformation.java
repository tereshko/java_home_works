import java.util.Scanner;

public class PrintStatusInformation {
    public PrintStatusInformation() {

    }

    public void printInfo(Chicken chicken) {
        System.out.println("-----------------------------------------------------");
        System.out.println("Seeds count: " + chicken.getSeedsCount());
        if (chicken.getSeedsCount() == 0) {
            System.out.println("Chicken will dead after next step, please feed");
        }
        System.out.println("Eggs count: " + chicken.getEggsCount());
        System.out.println("Is chicken available: " + chicken.getIsChickenAlive());
        System.out.println("Is egg available: " + chicken.getIsEggAvailable());
        System.out.println("Step count: " + chicken.getStepCount());
        System.out.println("-----------------------------------------------------");
    }

    public int getUserStepChoice() {
        boolean isAnswerCorrect = false;
        int userStepSelected = 0;

        while (!isAnswerCorrect) {
            System.out.println("1. Add 5 seeds");
            System.out.println("2. Take egg");
            System.out.println("3. Exit");
            System.out.print("Make Actions: ");

            Scanner in = new Scanner(System.in);
            userStepSelected = in.nextInt();
            if (userStepSelected > 0 && userStepSelected <= 3) {
                isAnswerCorrect = true;
            } else {
                System.out.println("Wrong answer");
            }
        }
        return userStepSelected;
    }
}
