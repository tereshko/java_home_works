import java.util.InputMismatchException;
import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        boolean valueA = false, valueB = false;

        System.out.println("Please enter boolean value A:");
        try {
            valueA = in.nextBoolean();
        } catch (InputMismatchException exception) {
            System.out.println("Wrong value A");
        }

        System.out.println("Please enter boolean value B:");
        try {
            valueB = in.nextBoolean();
        } catch (InputMismatchException exception) {
            System.out.println("Wrong value B");
        }

        System.out.println("inversion A = " + !valueA);
        System.out.println("inversion B = " + !valueB);

        System.out.println("A and B = " + (valueA && valueB));
        System.out.println("A or B = " + (valueA || valueB));

        System.out.println("inversion A and B = " + (!valueA || valueB));
        System.out.println("ekviva A and B = " + ((!valueA || valueB) && (valueA || !valueB)));
        System.out.println("A XOR B = " + (valueA ^ valueB));


    }
}
