import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Enter start: ");
        int start = in.nextInt();

        System.out.print("Enter end: ");
        int end = in.nextInt();

        System.out.print("Enter step: ");
        int step = in.nextInt();

        int currentValue = start;
        while (currentValue <= end){
            System.out.println("Value: " + currentValue);
            currentValue = currentValue + step;
        }
    }
}
